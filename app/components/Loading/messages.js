/*
 * Loading Messages
 *
 * This contains all the text for the Loading component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  body: {
    id: 'app.components.Loading.body',
    defaultMessage: 'Loading...',
  },
});
